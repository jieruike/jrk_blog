

function rand_color() {
    var skinArray = ["#F9EBEA", "#F5EEF8", "#D5F5E3","#E8F8F5","#F8F9F9","#FEF9E7","#F5EEF8","#F9E79F","#D7BDE2","#82E0AA","#85C1E9","#A3E4D7"];
    var color= skinArray[Math.floor(Math.random() * skinArray.length)];
    return color;
}

$(function () {
    document.onkeydown = document.onkeyup = document.onkeypress = function(event) {
        var e = event || window.event || arguments.callee.caller.arguments[0];
        if (e && e.keyCode == 123) {
            e.returnValue = false;
             let toastHTML = '<span>复制成功，请遵循本文的转载规则</span>';
             M.toast({html: toastHTML});
            return (false);
        }
    };
    document.oncontextmenu = new Function("return false;");
});
