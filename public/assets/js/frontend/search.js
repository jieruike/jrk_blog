var searchFunc = function (path, search_id, content_id) {
    'use strict';
    var $input = document.getElementById(search_id);
    var $resultContent = document.getElementById(content_id);
    $input.addEventListener('input', function () {
        var str = '<ul class=\"search-result-list\">';
        var keywords = this.value.trim().toLowerCase();
        $resultContent.innerHTML = "";
        if (this.value.trim().length <= 0) {
            return;
        }
        $.ajax({
            url:path,
            type: "GET",
            data: {"content":keywords},
            dataType: "json",
            success: function(data){
                if(data.code == 1){
                    //console.log(data);
                    data.data.forEach(function (da) {
                        var data_content = da.description.trim().replace(/<[^>]+>/g, "").toLowerCase();
                        var  index_content = data_content.indexOf(keywords);

                        var first_occur = index_content;
                        str += "<li><a href='" + da.url + "' class='search-result-title'>" + da.title + "</a>";
                        var content = da.description.trim().replace(/<[^>]+>/g, "");

                        var start = first_occur - 20;
                        var end = first_occur + 80;
                        if (start < 0) {
                            start = 0;
                        }
                        if (start == 0) {
                            end = 100;
                        }
                        if (end > content.length) {
                            end = content.length;
                        }
                        var match_content = content.substr(start, end);

                        var regS = new RegExp(keywords, "gi");
                        match_content = match_content.replace(regS, "<em class=\"search-keyword\">" + keywords + "</em>");
                        str += "<p class=\"search-result\">" + match_content + "...</p>"
                        str += "</li>";
                    });
                    str += "</ul>";
                    $resultContent.innerHTML = str;
                }else{
                    str += "<li><a href='javascript:' class='search-result-title'>"+data.msg+"</a></li></ul>";
                    $resultContent.innerHTML = str;
                }
            },
            error: function() {
                str += "<li><a href='javascript:' class='search-result-title'>错误</a></li></ul>";
                $resultContent.innerHTML = str;
            }
        })
    })
}