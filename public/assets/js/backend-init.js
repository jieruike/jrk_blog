define(['backend'], function (Backend) {

    require.config({
        paths: {
            'colorpicker': '../libs/colorpicker/bootstrap-colorpicker',
            'layui': '../libs/layui/layui',
        },
        shim: {
            'colorpicker': {
                deps: ['css!../libs/colorpicker/css/bootstrap-colorpicker.css'],
                init: function () {

                }
            },
            'layui': {
                deps: ['css!../libs/layui/css/layui.css'],
                init: function () {
                    return this.layui.config({dir: '/assets/libs/layui/'});
                }
            }
        }
    });


});