define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'clipboard'], function ($, undefined, Backend, Table, Form,ClipboardJS) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'auth.admin/index',
                    add_url: 'auth.admin/add',
                    edit_url: 'auth.admin/edit',
                    del_url: 'auth.admin/del',
                    multi_url: 'auth.admin/multi',
                }
            });

            var table = $("#table");

            //在表格内容渲染完成后回调的事件
            table.on('post-body.bs.table', function (e, json) {
                $("tbody tr[data-index]", this).each(function () {
                    if (parseInt($("td:eq(1)", this).text()) == Config.admin.id) {
                        
                        $("input[type=checkbox]", this).prop("disabled", true);
                    }
                });
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true,},
                        {field: 'id', title: 'ID'},
                        {field: 'username', title: __('Username'),events: Controller.api.events.copy,formatter: Controller.api.formatter.copy},
                        {field: 'nickname', title: __('Nickname')},
                        {
                            field: 'groups_text',
                            title: __('Group'),
                            operate: false,
                            formatter: Table.api.formatter.label
                        },
                        {field: 'email', title: __('Email')},
                        {field: 'status', title: __("Status"), formatter: Table.api.formatter.status},
                        {field: 'google_secret', title: __('谷歌验证秘钥'),events: Controller.api.events.copy,formatter: Controller.api.formatter.copy},
                        {field: 'google_auth', title: __("谷歌验证"), searchList: {"1":__('开启'),"0":__('关闭')}, formatter: Table.api.formatter.status},
                        {field: 'google_qrcode', title: __('谷歌二维码'), events: Table.api.events.image,formatter: Table.api.formatter.image},
                        {
                            field: 'logintime',
                            title: __('Login time'),
                            formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            sortable: true
                        },
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: function (value, row, index) {
                                if (row.id == Config.admin.id) {
                                    return '';
                                }
                                return Table.api.formatter.operate.call(this, value, row, index);
                            }
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            var rand=Fast.api.randomString(10)
            $("#email").val(rand+'@163.com');
            Form.api.bindevent($("form[role=form]"));
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter:{
                copy:function (value,row,index) {
                    return '<button class="btn btn-copy btn-xs bg-success" data-clipboard-text="'+value+'" data-original-title="点击复制" data-toggle="tooltip">'+value+'</button>'
                }
            },
            events:{
                copy:{
                    'click .btn-copy':function (e,value,row,index) {
                        var clipboard=new ClipboardJS(".btn");
                        Toastr.info("复制成功");
                    }
                }
            }
        }
    };
    return Controller;
});