define(['jquery', 'bootstrap', 'backend', 'table', 'form','clipboard'], function ($, undefined, Backend, Table, Form,ClipboardJS) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'article.mingju/index' + location.search,
                    add_url: 'article.mingju/add',
                    edit_url: 'article.mingju/edit',
                    del_url: 'article.mingju/del',
                    multi_url: 'article.mingju/multi',
                    table: 'mingju',
                }
            });

            var table = $("#table");

             //当表格数据加载完成时
             table.on('load-success.bs.table', function (e, data) {
                  /*if(data.html!=''&&data.html!=null){
                     $("#createhtml").html(data.html);
                   }*/
              });

            // 初始化表格
            //,events: Controller.api.events.copy, formatter: Controller.api.formatter.copy
            //,sortable:true  ,visible:false  , operate:'LIKE %...%'  ,custom:{'0':'success','1':'danger'}
            //,placeholder: '关键字，模糊搜索', operate:'FIND_IN_SET'
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                //searchFormVisible: true, //是否始终显示搜索表单
                //fixedColumns: true,
                //fixedRightNumber: 1,
                //pageSize: 15,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'type', title: __('Type')},
                        {field: 'content', title: __('Content')},
                        {field: 'author', title: __('Author')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
           bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
           },
           formatter:{
                copy:function (value,row,index) {
                    var width = this.width != undefined ? (this.width.match(/^\d+$/) ? this.width + "px" : this.width) : "250px";
                    var copy_but=__('Click copy');
                    if (value=="" ||value==null ||value =='null') value ='-';
                        return '<button class="btn btn-copy btn-xs bg-success" data-clipboard-text="'+value+'" data-original-title="'+copy_but+'" data-toggle="tooltip">' +
                               '<div style="white-space: nowrap; text-overflow:ellipsis; overflow: hidden; max-width:"'+ width +'";>'+ value +' </div>' +
                                '</button>';
                }
           },
           events:{
               copy:{
                    'click .btn-copy':function (e,value,row,index) {
                          var clipboard=new ClipboardJS(".btn");
                           Toastr.info(__("Copy successfully"));
                     }
                }
           }
         }
    };
    return Controller;
});