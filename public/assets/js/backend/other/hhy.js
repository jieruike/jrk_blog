define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            //  Controller.api.bindevent()
            require(['upload'], function (Upload) {
                Upload.api.plupload($(".btn-import"), function (data, ret) {
                    Fast.api.ajax({
                        url: 'other.hhy/fileup',
                        data: {file: data.url},
                    }, function (data, ret) {
                        // console.log(ret)
                        var phone=$("#phones");
                        phone.val(ret.data)
                        phone.prop("readonly",true)
                    });
                });
            });


            LimitedNnumber("#content", "#viewBox", 1000)//调用函数需要传入三个参数，分别为，输入框、显示框、限制的长度（这里是9）
            function LimitedNnumber(eventBox, viewBox, textLength) {//调用函数需要传入三个参数，分别为，输入框、显示框、限制的长度
                $(document).on('input propertychange paste keyup', eventBox, function(event) {
                    this.value = this.value.replace(this.value.slice(textLength), "")//超出长度的部分替换为空
                    $(viewBox).text(this.value.length)
                })
            }

            Form.api.bindevent($("form[role=form]"), function (data, ret) {
                location.reload();
            });


        },
    };
    return Controller;
});
