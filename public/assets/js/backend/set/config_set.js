define(['jquery', 'bootstrap', 'backend', 'table', 'form','layui'], function ($, undefined, Backend, Table, Form,Layui) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'set.config_set/index' + location.search,
                    add_url: 'set.config_set/add',
                    edit_url: 'set.config_set/edit',
                    del_url: 'set.config_set/del',
                    multi_url: 'set.config_set/multi',
                    table: 'hhy_config_set',
                }
            });
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'title', title: __('Title')},
                        {field: 'message', title: __('Message')},
                        {field: 'icon', title: __('Icon'), searchList: {"huodong":__('Huodong'),"order-icon":__('Order-icon'),"score-icon":__('Score-icon'),"services-icon":__('Services-icon'),"share-icon":__('Share-icon'),"shopro-icon":__('Shopro-icon'),"user-icon":__('User-icon'),"wallet-icon":__('Wallet-icon'),"withdraw-icon":__('Withdraw-icon')}},
                        {field: 'background', title: __('Background'),formatter:function (value) {
                                return '<span style="background-color: '+value+'">'+value+'</span>';
                            }},
                        {field: 'background_button', title: __('Background_button'),formatter:function (value) {
                                return '<span style="background-color: '+value+'">'+value+'</span>';
                            }},
                        {field: 'background_color', title: __('Background_color'),formatter:function (value) {
                                return '<span style="background-color: '+value+'">'+value+'</span>';
                            }},
                        {field: 'status', title: __('Status'), searchList: {"0":__('Status 0'),"1":__('Status 1')}, formatter: Table.api.formatter.toggle},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();

        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                   Layui.use(['form', 'upload','colorpicker'], function (form, upload) {
                        var q = layui.$
                            ,colorpicker = layui.colorpicker;
                        //背景颜色
                       colorpicker.render({
                           elem: '#c-background'
                           ,done: function(color){
                               $('#c-background-value').val(color);
                           }
                       });
                       colorpicker.render({
                           elem: '#c-background_button'
                           ,done: function(color){
                               $('#c-background_button-value').val(color);
                           }
                       });
                       colorpicker.render({
                           elem: '#c-background_color'
                           ,done: function(color){
                               $('#c-background_color-value').val(color);
                           }
                       });
                    });
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});