define(['jquery', 'bootstrap', 'backend', 'table', 'form','clipboard'], function ($, undefined, Backend, Table, Form,ClipboardJS) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'set.country/index' + location.search,
                    add_url: 'set.country/add',
                    edit_url: 'set.country/edit',
                    del_url: 'set.country/del',
                    multi_url: 'set.country/multi',
                    table: 'country',
                }
            });

            var table = $("#table");

             //当表格数据加载完成时
             table.on('load-success.bs.table', function (e, data) {

              });

            // 初始化表格
            //,events: Controller.api.events.copy, formatter: Controller.api.formatter.copy
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),sortable:true},
                        {field: 'e_name', title: __('E_name'),events: Controller.api.events.copy, formatter: Controller.api.formatter.copy},
                        {field: 'code', title: __('Code'),events: Controller.api.events.copy, formatter: Controller.api.formatter.copy},
                        {field: 'c_name', title: __('C_name'),events: Controller.api.events.copy, formatter: Controller.api.formatter.copy},
                        {field: 's_name', title: __('S_name'),events: Controller.api.events.copy, formatter: Controller.api.formatter.copy},
                        {field: 'status', title: __('Status'), searchList: {"1":__('Status 1'),"0":__('Status 0')}, formatter: Table.api.formatter.status},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
                   bindevent: function () {
                       Form.api.bindevent($("form[role=form]"));
                   },
                   formatter:{
                       copy:function (value,row,index) {
                           var width = this.width != undefined ? (this.width.match(/^\d+$/) ? this.width + "px" : this.width) : "250px";
                           if (value=="" ||value==null ||value =='null') value ='-';
                           return '<button class="btn btn-copy btn-xs bg-success" data-clipboard-text="'+value+'" data-original-title="点击复制" data-toggle="tooltip">' +
                               '<div style="white-space: nowrap; text-overflow:ellipsis; overflow: hidden; max-width:"'+ width +'";>'+ value +' </div>' +
                               '</button>';
                       }
                   },
                   events:{
                       copy:{
                           'click .btn-copy':function (e,value,row,index) {
                               var clipboard=new ClipboardJS(".btn");
                               Toastr.info("复制成功");
                           }
                       }
                   }
         }
    };
    return Controller;
});