define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            var configIndex = new Vue({
                el: "#configIndex",
                data() {
                    return {
                        configData: Config.config_list
                    }
                },
                mounted() {
                },
                methods: {
                    operation(name, title) {
                        let that = this;
                        Fast.api.open("general.hhy_config/platform?type=" + name, title);
                    },
                    tabClick(tab, event) {
                        this.activeName = tab.name;
                    },
                },
            })
            /*console.log(Config.config_list);*/
        },
        platform: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});