define(['jquery', 'bootstrap', 'backend', 'table', 'form','clipboard'], function ($, undefined, Backend, Table, Form,ClipboardJS) {
    const searchlist = function () {
        return new Promise(resolve => {
            $.getJSON('other/country/selectpages').done(res => {
                resolve(res)
            })
        })
    };
    var Controller = {
        index: async function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'agent/dai_channel/index' + location.search,
                    table: 'pay_for_another',
                }
            });

            var table = $("#table");
            var urlArr = [];
            table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function (e, row) {
                if (e.type == 'check' || e.type == 'uncheck') {
                    row = [row];
                } else {
                    urlArr = [];
                }
                $.each(row, function (i, j) {
                    if (e.type.indexOf("uncheck") > -1) {
                        var index = urlArr.indexOf(j.mer_no);
                        if (index > -1) {
                            urlArr.splice(index, 1);
                        }
                    } else {
                        urlArr.indexOf(j.mer_no) == -1 && urlArr.push(j.mer_no);
                    }
                });
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showToggle: false,
                showExport: false,
                maintainSelected: true,
                columns: [
                    [
                        {checkbox: true, visible: true, operate: false},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'mer_no', title: __('商户号'), operate: 'LIKE'},
                        {field: 'status', title: __('Status'), searchList: {"0":__('关闭'),"1":__('启用')}, formatter: Table.api.formatter.status},
                        {field: 'country_ids', title: __('所属国家'), operate:'FIND_IN_SET',searchList: await searchlist(),formatter: Table.api.formatter.label},
                        {field: 'remark', title: __('备注'), operate: 'LIKE'},
                        {
                            field: 'operate', title: __('Operate'), events: {
                                'click .btn-chooseone': function (e, value, row, index) {
                                    Fast.api.close({url: row.mer_no, multiple: true});
                                },
                            }, formatter: function () {
                                return '<a href="javascript:;" class="btn btn-danger btn-chooseone btn-xs"><i class="fa fa-check"></i> ' + __('Choose') + '</a>';
                            }
                        }
                    ]
                ]
            });

            // 选中多个
            $(document).on("click", ".btn-choose-multi", function () {
                Fast.api.close({url: urlArr.join(","), multiple: true});
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});


/*   //入金渠道选择
            $(".btn-rujin").click(function () {
                    parent.Fast.api.open('agent/pay_channel/index', __('选择入金渠道'), {
                        callback: function (data) {
                            var button = $("#ruqudao");
                            var maxcount = $(button).data("maxcount");
                            var input_id = $(button).data("input-id") ? $(button).data("input-id") : "";
                            maxcount = typeof maxcount !== "undefined" ? maxcount : 0;
                            if (input_id && data.multiple) {
                                var urlArr = [];
                                var inputObj = $("#" + input_id);
                                var value = $.trim(inputObj.val());
                                if (value !== "") {
                                    urlArr.push(inputObj.val());
                                }
                                if (data.url.length >0){
                                    urlArr.push(data.url)
                                }
                                var result = urlArr.join(",");
                                if (maxcount > 0) {
                                    var leng=result.split(',');
                                    if (leng.length> maxcount) {
                                        Toastr.error(__('你最多可以选择 %s 个入金渠道', maxcount));
                                        return false;
                                    }
                                }
                                inputObj.val(result).trigger("change").trigger("validate");
                            } else {
                                $("#" + input_id).val(data.url).trigger("change").trigger("validate");
                            }
                        }
                    });
               });
*/