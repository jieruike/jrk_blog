<?php

namespace addons\poster\model;

use app\common\model\BaseModel;


class Poster extends BaseModel
{

    // 表名
    protected $name = 'poster';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    // 字段设置类型自动转换，会在写入和读取的时候自动进行类型转换处理
    protected $type = [
        'avatar_size' => 'json',
        'avatar_position' => 'json',
        'qrcode_size' => 'json',
        'qrcode_position' => 'json',
        'font_size' => 'json',
        'font_position' => 'json',
        'nickname_size' => 'json',
        'nickname_position' => 'json',
    ];

    // 追加属性
    protected $append = [
        'status_text'
    ];

    public function getStatusList()
    {
        return ['hidden' => __('隐藏'), 'normal' => __('正常')];
    }

    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
