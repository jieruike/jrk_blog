<?php


namespace addons\poster\logic;



use addons\poster\model\Poster;
use app\common\model\User;
use Endroid\QrCode\QrCode;
use fast\Http;

/**
 * Class PosterLogic
 *
 * @package addons\poster\logic
 */
class PosterLogic
{
    /**
     * 海报列表
     *
     * @return bool|\think\Collection
     */
    public static function getPosterList()
    {
        try{
            $list = Poster::where('status','normal')->order('weigh desc')->select();
        }catch (\Exception $exception){
            return false;
        }
        if ($list->isEmpty()){
            return false;
        }
        return $list;
    }

    /**
     * 获取用户海报
     *
     * @param  User  $user
     *
     * @return array
     */
    public static function getUserPoster($user_id=0,$text='',$code='',$avatar='',$nickname='')
    {
        //用户二维码
        $qrcode = self::generateQrcodePicture($text, 'user', $user_id);
        if (!$qrcode){
            return false;
        }
        $arr = parse_url($qrcode);
        //二维码资源路径
        $qrcodePath = root_path().'public'.$arr['path'];
        //dump($qrcode,$arr,$qrcodePath);

        //生成的海报
        $data=[];
        //海报
        $list = self::getPosterList();
        if (!$list){
            return false;
        }

        foreach ($list as $poster) {
            //dump($poster->qrcode_size);
            if (empty($poster->qrcode_size) || empty($poster->qrcode_position)) {
                continue;
            }


            //背景图
            $qrcode_bg = cdnurl($poster['image'], true);

            //海报未知
            $path_1 = root_path('public');
            $path_2 = 'qrcodes/poster/'.$poster->id.'/'.$user_id;
            $path_3 = '/'.$user_id.'_'.$poster->id.'.jpg';
            $filename = $path_1.$path_2.$path_3;
            $filename2 = '/'.$path_2.$path_3;

            //检测用户海报是否存在
            if (file_exists($filename)) {
                //已存在
                $data[] = cdnurl($filename2, true);
                continue;
            }

            //生成海报
            $config = [];
            //try {
            $config['image'][] = [
                'url'     => $qrcodePath,     //二维码资源
                'stream'  => 0,
                'left'    => $poster->qrcode_position['x'],
                'top'     => $poster->qrcode_position['y'],
                'right'   => 0,
                'bottom'  => 0,
                'width'   => $poster->qrcode_size['w'],
                'height'  => $poster->qrcode_size['h'],
                'opacity' => 100
            ];


            //展示头像
            if ($poster->is_avatar!=0 && $avatar){
                if (strpos($avatar,'http')!== false){
                    $save_to = $path_1.'qrcodes/avatar/';
                    if (! file_exists($save_to)) {
                        //生成文件夹
                        mkdir($save_to, 0777, true);
                    }
                    $content = Http::get($avatar);
                    file_put_contents($save_to.$user_id.'.jpg', $content);
                    $avatarPath =$save_to.$user_id.'.jpg';
                }else{
                    $avatarPath = root_path().'public'.$avatar;
                }
                if ($poster->avatar_size && $poster->avatar_position && file_exists($avatarPath)){
                    $config['image'][] =
                        [
                            'url'     => $avatarPath,     //头像资源
                            'stream'  => 0,
                            'left'    => $poster->avatar_position['x'],
                            'top'     => $poster->avatar_position['y'],
                            'right'   => 0,
                            'bottom'  => 0,
                            'width'   => $poster->avatar_size['w'],
                            'height'  => $poster->avatar_size['h'],
                            'opacity' => 100
                        ]
                    ;
                }
            }

            $config['background'] = $qrcode_bg;


            if ($poster->is_font != 0 && $code) {
                //带邀请码
                $config['text'][] = [
                    'text'      => $code,//换成你的邀请码
                    'left'      => $poster->font_position['x'],
                    'top'       => $poster->font_position['y'],
                    'fontPath'  => root_path().'public/assets/fonts/SourceHanSansK-Regular.ttf',     //字体文件
                    'fontSize'  => $poster->font_size['size'],             //字号
                    'fontColor' => $poster->font_size['color'],       //字体颜色
                    'angle'     => 0,
                ];
            }

            if ($poster->is_nickname != 0 && $nickname) {
                //用户昵称
                $config['text'][] = [
                    'text'      => $nickname,
                    'left'      => $poster->nickname_position['x'],
                    'top'       => $poster->nickname_position['y'],
                    'fontPath'  => root_path().'public/assets/fonts/SourceHanSansK-Regular.ttf',     //字体文件
                    'fontSize'  => $poster->nickname_size['size'],             //字号
                    'fontColor' => $poster->nickname_size['color'],       //字体颜色
                    'angle'     => 0,
                ];

            }
            //halt($config);
            if (! $config) {
                continue;
            }

            $fileDir = $path_1.$path_2;
            if (! file_exists($fileDir)) {
                //生成文件夹
                mkdir($fileDir, 0777, true);
            }

            $res = self::createPoster($config, $filename);

            //halt(11111,$filename,$data,$res);
            if ($res) {
                //检测用户海报是否存在
                if (file_exists($res)) {
                    //存在
                    $data[] = cdnurl($filename2, true);
                    continue;
                }
                $data[] = cdnurl($filename2, true);
            }
        }

        return $data;
    }



    /**
     * 生成二维码
     * @param  string  $text
     * @param  string  $tmpDir
     * @param  string  $filename
     * @param  int  $size
     * @param  int  $padding
     * @param  int  $is_cover
     *
     * @return string
     */
    public static function generateQrcodePicture($text='',$tmpDir='',$filename='',$size=200,$padding=0,$is_cover=0)
    {
        //二维码目录
        $qrcodePath = $tmpDir?'/qrcodes/'.$tmpDir:'/qrcode';
        //文件名
        $filename = $filename?:uniqid();
        //二维码url
        $uqr = request()->domain(). $qrcodePath.'/'.$filename.'.png';

        $pathDir = 'public'.$qrcodePath;
        $path = root_path($pathDir);
        $qrcodeDir = $path.$filename.'.png';
        if ($is_cover==0){
            //不强制覆盖
            if (file_exists($qrcodeDir)) {
                //已存在
                return $uqr;
            }
        }
        if (! file_exists($path)) {
            //生成文件夹
            mkdir($path, 0777, true);
        }
        $result = false;
        try{
            $qrCode = new QrCode();
            $qrCode->setText($text)
                ->setSize($size)
                ->setPadding($padding)
                ->setImageType(QrCode::IMAGE_TYPE_PNG);

            //>>>>>>>>>保存文件>>>>>>>>>>>
            $result = $qrCode->save($qrcodeDir); //保存文件
        }catch (\Exception $exception){

        }
        return $result?$uqr:'';
    }


    /**
     * 生成宣传海报
     *
     * @param  array  $config  参数,包括图片和文字
     * @param  string  $filename  生成海报文件名,不传此参数则不生成文件,直接输出图片
     *
     * @return bool|string
     */
    public static function createPoster($config = [], $filename = "")
    {
        //如果要看报什么错，可以先注释调这个header
        //if(empty($filename)) header("content-type: image/png");
        $imageDefault = array(
            'left'    => 0,
            'top'     => 0,
            'right'   => 0,
            'bottom'  => 0,
            'width'   => 100,
            'height'  => 100,
            'opacity' => 100
        );
        $textDefault = array(
            'text'      => '',
            'left'      => 0,
            'top'       => 0,
            'fontSize'  => 32,       //字号
            'fontColor' => '255,255,255', //字体颜色
            'angle'     => 0,
        );
        $background = $config['background'];//海报最底层得背景
        //背景方法
        $backgroundInfo = getimagesize($background);
        $backgroundFun = 'imagecreatefrom'.image_type_to_extension($backgroundInfo[2], false);
        $background = $backgroundFun($background);
        $backgroundWidth = imagesx($background);  //背景宽度
        $backgroundHeight = imagesy($background);  //背景高度
        $imageRes = imageCreatetruecolor($backgroundWidth, $backgroundHeight);
        $color = imagecolorallocate($imageRes, 0, 0, 0);
        imagefill($imageRes, 0, 0, $color);
        // imageColorTransparent($imageRes, $color);  //颜色透明
        imagecopyresampled($imageRes, $background, 0, 0, 0, 0, imagesx($background), imagesy($background),
            imagesx($background), imagesy($background));
        //处理了图片
        if (! empty($config['image'])) {
            foreach ($config['image'] as $key => $val) {
                $val = array_merge($imageDefault, $val);

                $info = getimagesize($val['url']);
                $function = 'imagecreatefrom'.image_type_to_extension($info[2], false);
                if ($val['stream']) {   //如果传的是字符串图像流
                    $info = getimagesizefromstring($val['url']);
                    $function = 'imagecreatefromstring';
                }
                $res = $function($val['url']);
                $resWidth = $info[0];
                $resHeight = $info[1];
                //建立画板 ，缩放图片至指定尺寸
                $canvas = imagecreatetruecolor($val['width'], $val['height']);
                imagefill($canvas, 0, 0, $color);
                //关键函数，参数（目标资源，源，目标资源的开始坐标x,y, 源资源的开始坐标x,y,目标资源的宽高w,h,源资源的宽高w,h）
                imagecopyresampled($canvas, $res, 0, 0, 0, 0, $val['width'], $val['height'], $resWidth, $resHeight);
                $val['left'] = $val['left'] < 0 ? $backgroundWidth - abs($val['left']) - $val['width'] : $val['left'];
                $val['top'] = $val['top'] < 0 ? $backgroundHeight - abs($val['top']) - $val['height'] : $val['top'];
                //放置图像
                imagecopymerge($imageRes, $canvas, $val['left'], $val['top'], $val['right'], $val['bottom'],
                    $val['width'], $val['height'], $val['opacity']);//左，上，右，下，宽度，高度，透明度
            }
        }
        //处理文字
        if (! empty($config['text'])) {
            foreach ($config['text'] as $key => $val) {
                $val = array_merge($textDefault, $val);
                list($R, $G, $B) = explode(',', $val['fontColor']);
                $fontColor = imagecolorallocate($imageRes, $R, $G, $B);
                $val['left'] = $val['left'] < 0 ? $backgroundWidth - abs($val['left']) : $val['left'];
                $val['top'] = $val['top'] < 0 ? $backgroundHeight - abs($val['top']) : $val['top'];
                imagettftext($imageRes, $val['fontSize'], $val['angle'], $val['left'], $val['top'], $fontColor,
                    $val['fontPath'], $val['text']);
            }
        }
        //生成图片
        if (! empty($filename)) {
            $res = imagejpeg($imageRes, $filename, 90); //保存到本地
            imagedestroy($imageRes);
            if (! $res) {
                return false;
            }
            return $filename;
        } else {
            imagejpeg($imageRes);     //在浏览器上显示
            imagedestroy($imageRes);
        }
    }


}