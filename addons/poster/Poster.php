<?php

namespace addons\poster;

use app\common\library\Menu;
use think\Addons;

/**
 * 海报生成
 */
class Poster extends Addons
{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        $menu = [
            [
                'name'    => 'poster',
                'title'   => '海报管理',
                'icon'    => 'fa fa-image',
                'sublist' => [
                    ['name' => 'poster/index', 'title' => '查看'],
                    ['name' => 'poster/add', 'title' => '添加'],
                    ['name' => 'poster/edit', 'title' => '编辑'],
                    ['name' => 'poster/del', 'title' => '删除'],
                    ['name' => 'poster/multi', 'title' => '批量更新'],
                    ['name' => 'poster/clear', 'title' => '清空已生成海报'],
                ]
            ]
        ];
        Menu::create($menu);
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        Menu::delete('poster');
        return true;
    }

    /**
     * 插件启用方法
     * @return bool
     */
    public function enable()
    {
        Menu::enable('poster');
        return true;
    }

    /**
     * 插件禁用方法
     * @return bool
     */
    public function disable()
    {
        Menu::disable('poster');
        return true;
    }

}
