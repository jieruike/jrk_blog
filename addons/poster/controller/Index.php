<?php

namespace addons\poster\controller;

use addons\poster\logic\PosterLogic;
use think\addons\Controller;


/**
 * 二维码生成
 *
 */
class Index extends Controller
{

    protected $noNeedLogin = [''];

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
        $user = $this->auth->getUser();
        //二维码内容
        $text = request()->domain();
        $res  = PosterLogic::getUserPoster($user->id,$text,$user->salt,$user->avatar,$user->nickname);
        $data = $res ? : [];
        return json(['code'=>1,'msg'=>'ok','data'=>$data]);
    }


}
