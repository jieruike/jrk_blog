
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fa_poster
-- ----------------------------
DROP TABLE IF EXISTS `__PREFIX__poster`;
CREATE TABLE `fa_poster`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '海报名称',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '背景图片',
  `qrcode_size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '二维码大小{\"w\":\"\",\"h\":\"\"}',
  `qrcode_position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '二维码坐标{\"x\":\"\",\"y\":\"\"}',
  `is_avatar` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否显示头像:0=否,1=是',
  `avatar_size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像大小{\"w\":\"\",\"h\":\"\"}',
  `avatar_position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像坐标{\"x\":\"\",\"y\":\"\"}',
  `is_font` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否显示邀请码:0=否,1=是',
  `font_size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邀请码设置{\"size\":\"大小\",\"color\":\"r,g,b\"}',
  `font_position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邀请码坐标{\"x\":\"\",\"y\":\"\"}',
  `is_nickname` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否显示昵称:0=否,1=是',
  `nickname_size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称设置{\"size\":\"大小\",\"color\":\"r,g,b\"}',
  `nickname_position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称坐标{\"x\":\"\",\"y\":\"\"}',
  `weigh` int(11) NOT NULL DEFAULT 0 COMMENT '权重',
  `status` enum('hidden','normal') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'hidden' COMMENT '状态:hidden=隐藏,normal=正常',
  `createtime` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updatetime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '海报管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_poster
-- ----------------------------
INSERT INTO `fa_poster` VALUES (1, '测试海报1', '/assets/addons/poster/img/poster1.png', '{\"h\": \"330\", \"w\": \"330\"}', '{\"x\": \"375\", \"y\": \"-60\"}', 1, '{\"h\": \"100\", \"w\": \"100\"}', '{\"x\": \"477\", \"y\": \"-505\"}', 1, '{\"size\": \"28\", \"color\": \"255,255,255,1\"}', '{\"x\": \"465\", \"y\": \"-454\"}', 1, '{\"size\": \"28\", \"color\": \"255,255,255,1\"}', '{\"x\": \"465\", \"y\": \"-650\"}', 1, 'normal', 1590635294, 1590635294);
INSERT INTO `fa_poster` VALUES (2, '测试海报2', '/assets/addons/poster/img/poster2.png', '{\"h\": \"330\", \"w\": \"330\"}', '{\"x\": \"380\", \"y\": \"-270\"}', 0, '', '', 1, '{\"size\": \"28\", \"color\": \"255,11,17\"}', '{\"x\": \"485\", \"y\": \"-110\"}', 0, '', '', 2, 'normal', 1590635294, 1590635294);

SET FOREIGN_KEY_CHECKS = 1;
