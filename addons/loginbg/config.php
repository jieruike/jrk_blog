<?php

return [
    0 => [
        'name' => 'mode',
        'title' => '模式',
        'type' => 'radio',
        'content' => [
            'fixed' => '固定',
            'random' => '每次随机',
            'daily' => '每日切换',
        ],
        'value' => 'daily',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    1 => [
        'name' => 'image',
        'title' => '固定背景图',
        'type' => 'image',
        'content' => [
        ],
        'value' => '/uploads/images/20210630/67f04052c56443d8721804482e80df08.jpg',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
];
