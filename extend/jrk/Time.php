<?php

namespace jrk;

class Time{
    /**
     * @param string $type
     * @param bool $date
     * @param string $format
     * @return array
     *
     * @describe:获取开始和结束时间
     */
    public static function TimeBeginEnd($type = "today", $date = true, $format = "Y-m-d H:i:s") {
        switch (strtolower($type)) {
            case "tomorrow":  //明天的开始和结束时间
                $arr = [self::BeginTomorrow($date, $format), self::EndTomorrow($date, $format)];
                break;
            case "yesterday": //昨天的开始和结束时间
                $arr = [self::BeginYesterday($date, $format), self::EndYesterday($date, $format)];
                break;
            case "thisweek":  //本周的开始和结束时间 【开始时间周一，结束时间为周日】
                $arr = [self::BeginThisWeek($date, $format), self::EndThisWeek($date, $format)];
                break;
            case "thisweekr":  //本周的开始和结束时间【开始时间周日，结束时间为周六】
                $arr = [self::BeginThisWeekR($date, $format), self::EndThisWeekR($date, $format)];
                break;
            case "lastweek":  //上周的开始和结束时间
                $arr = [self::BeginLastWeek($date, $format), self::EndLastWeek($date, $format)];
                break;
            case "thismonth":  //本月的开始和结束时间
                $arr = [self::BeginThisMonth($date, $format), self::EndThisMonth($date, $format)];
                break;
            case "lastmonth":  //上月的开始和结束时间
                $arr = [self::BeginLastMonth($date, $format), self::EndLastMonth($date, $format)];
                break;
            case "thisseason": //本季度的开始和结束时间
                $arr = [self::BeginThisSeason($date, $format), self::EndThisSeason($date, $format)];
                break;
            case "lastseason":  //上季度的开始和结束时间
                $arr = [self::BeginLastSeason($date, $format), self::EndLastSeason($date, $format)];
                break;
            case "thisyear":  //今年的开始和结束时间
                $arr = [self::BeginThisYear($date, $format), self::EndThisYear($date, $format)];
                break;
            case "tomorrowyear": //明年的开始和结束时间
                $arr = [self::BeginTomorrowYear($date, $format), self::EndTomorrowYear($date, $format)];
                break;
            case "lastyear": //去年的开始和结束时间
                $arr = [self::BeginLastYear($date, $format), self::EndLastYear($date, $format)];
                break;
            default:   //今天的开始和结束时间
                $arr = [self::BeginToday($date, $format), self::EndToday($date, $format)];
                break;
        }
        return $arr;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:今日开始时间
     */
    public static function BeginToday($date = true, $format = "Y-m-d H:i:s") {
        $today = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        return $date ? date($format, $today) : $today;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:今日结束时间
     */
    public static function EndToday($date = true, $format = "Y-m-d H:i:s") {
        $today = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
        return $date ? date($format, $today) : $today;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:明日开始时间
     */
    public static function BeginTomorrow($date = true, $format = "Y-m-d H:i:s") {
        $Tomorrow = mktime(0, 0, 0, date('m'), date('d'), date('Y')) + 86400;
        return $date ? date($format, $Tomorrow) : $Tomorrow;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:明日结束时间
     */
    public static function EndTomorrow($date = true, $format = "Y-m-d H:i:s") {
        $Tomorrow = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) + 86399;
        return $date ? date($format, $Tomorrow) : $Tomorrow;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:昨日开始时间
     */
    public static function BeginYesterday($date = true, $format = "Y-m-d H:i:s") {
        $Yesterday = mktime(0, 0, 0, date('m'), date('d') - 1, date('Y'));
        return $date ? date($format, $Yesterday) : $Yesterday;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:昨日结束时间
     */
    public static function EndYesterday($date = true, $format = "Y-m-d H:i:s") {
        $Yesterday = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 1;
        return $date ? date($format, $Yesterday) : $Yesterday;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:本周开始时间 【开始时间周一，结束时间为周日】
     */
    public static function BeginThisWeek($date = true, $format = "Y-m-d H:i:s") {
        $ThisWeek = strtotime("this week Monday", time());
        return $date ? date($format, $ThisWeek) : $ThisWeek;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:本周结束时间 【开始时间周一，结束时间为周日】
     */
    public static function EndThisWeek($date = true, $format = "Y-m-d H:i:s") {
        $ThisWeek = strtotime("this week Sunday", time()) + 86399;
        return $date ? date($format, $ThisWeek) : $ThisWeek;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:本周开始时间 【开始时间周日，结束时间为周六】
     */
    public static function BeginThisWeekR($date = true, $format = "Y-m-d H:i:s") {
        $ThisWeek = mktime(0, 0, 0, date("m"), date("d") - date("w") + 1, date("Y"));
        return $date ? date($format, $ThisWeek) : $ThisWeek;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:本周结束时间 【开始时间周日，结束时间为周六】
     */
    public static function EndThisWeekR($date = true, $format = "Y-m-d H:i:s") {
        $ThisWeek = mktime(0, 0, 0, date("m"), date("d") - date("w") + 7, date("Y"));
        return $date ? date($format, $ThisWeek) : $ThisWeek;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:上周开始时间
     */
    public static function BeginLastWeek($date = true, $format = "Y-m-d H:i:s") {
        $LastWeek = strtotime("last week Monday", time());
        return $date ? date($format, $LastWeek) : $LastWeek;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:上周结束时间
     */
    public static function EndLastWeek($date = true, $format = "Y-m-d H:i:s") {
        $LastWeek = strtotime("last week Sunday", time()) + 86399;
        return $date ? date($format, $LastWeek) : $LastWeek;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:这月开始时间
     */
    public static function BeginThisMonth($date = true, $format = "Y-m-d H:i:s") {
        $ThisMonth = mktime(0, 0, 0, date('m'), 1, date('Y'));
        return $date ? date($format, $ThisMonth) : $ThisMonth;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:这月结束时间
     */
    public static function EndThisMonth($date = true, $format = "Y-m-d H:i:s") {
        $ThisMonth = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
        return $date ? date($format, $ThisMonth) : $ThisMonth;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:上月开始时间
     */
    public static function BeginLastMonth($date = true, $format = "Y-m-d H:i:s") {
        $LastMonth = mktime(0, 0, 0, date("m") - 1, 1, date("Y"));
        return $date ? date($format, $LastMonth) : $LastMonth;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:上月结束时间
     */
    public static function EndLastMonth($date = true, $format = "Y-m-d H:i:s") {
        $LastMonth = mktime(23, 59, 59, date("m"), 0, date("Y"));
        return $date ? date($format, $LastMonth) : $LastMonth;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:本季度开始时间
     */
    public static function BeginThisSeason($date = true, $format = "Y-m-d H:i:s") {
        $season = ceil((date('n')) / 3);
        $ThisSeason = mktime(0, 0, 0, $season * 3 - 3 + 1, 1, date('Y'));
        return $date ? date($format, $ThisSeason) : $ThisSeason;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:本季度结束时间
     */
    public static function EndThisSeason($date = true, $format = "Y-m-d H:i:s") {
        $season = ceil((date('n')) / 3);
        $ThisSeason = mktime(23, 59, 59, $season * 3, date('t', mktime(0, 0, 0, $season * 3, 1, date("Y"))), date('Y'));
        return $date ? date($format, $ThisSeason) : $ThisSeason;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:上季度开始时间
     */
    public static function BeginLastSeason($date = true, $format = "Y-m-d H:i:s") {
        $season = ceil((date('n')) / 3) - 1;
        $ThisSeason = mktime(0, 0, 0, $season * 3 - 3 + 1, 1, date('Y'));
        return $date ? date($format, $ThisSeason) : $ThisSeason;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:上季度结束时间
     */
    public static function EndLastSeason($date = true, $format = "Y-m-d H:i:s") {
        $season = ceil((date('n')) / 3) - 1;
        $ThisSeason = mktime(23, 59, 59, $season * 3, date('t', mktime(0, 0, 0, $season * 3, 1, date("Y"))), date('Y'));
        return $date ? date($format, $ThisSeason) : $ThisSeason;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:今年开始时间
     */
    public static function BeginThisYear($date = true, $format = "Y-m-d H:i:s") {
        $ThisYear = mktime(0, 0, 0, 1, 1, date('Y'));
        return $date ? date($format, $ThisYear) : $ThisYear;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:今年结束时间
     */
    public static function EndThisYear($date = true, $format = "Y-m-d H:i:s") {
        $ThisYear = mktime(23, 59, 59, 12, 31, date('Y'));
        return $date ? date($format, $ThisYear) : $ThisYear;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:明年开始时间
     */
    public static function BeginTomorrowYear($date = true, $format = "Y-m-d H:i:s") {
        $TomorrowYear = mktime(0, 0, 0, 1, 1, date('Y') + 1);
        return $date ? date($format, $TomorrowYear) : $TomorrowYear;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:明年结束时间
     */
    public static function EndTomorrowYear($date = true, $format = "Y-m-d H:i:s") {
        $TomorrowYear = mktime(23, 59, 59, 12, 31, date('Y') + 1);
        return $date ? date($format, $TomorrowYear) : $TomorrowYear;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:去年开始时间
     */
    public static function BeginLastYear($date = true, $format = "Y-m-d H:i:s") {
        $LastYear = mktime(0, 0, 0, 1, 1, date('Y') - 1);
        return $date ? date($format, $LastYear) : $LastYear;
    }


    /**
     * @param bool $date
     * @param string $format
     * @return false|int|string
     *
     * @describe:去年结束时间
     */
    public static function EndLastYear($date = true, $format = "Y-m-d H:i:s") {
        $LastYear = mktime(23, 59, 59, 12, 31, date('Y') - 1);
        return $date ? date($format, $LastYear) : $LastYear;
    }

}