<h1 align="center">JrkBlog_TP6.0简易博客 —— 你值得拥有</h1> 
<p align="center">
<img src="https://gitee.com/luckygyl/jrk_blog/badge/star.svg?theme=dark"  /> 
<img src="https://gitee.com/luckygyl/jrk_blog/badge/fork.svg?theme=dark"  /> 
<a href="http://www.php.net/" target="_blank">
<img src="https://img.shields.io/badge/php-%3E%3D7.1-8892BF.svg"  /> 
</a>
</p>
<p align="center">    
    <b>如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！</b>
</p>

### 项目介绍
JrkBlog博客基于YFCMF后台开发<br/>
完善如下功能：<br/>
phpquerylist数据采集，
phpword导出word文档，
实用好用的API接口，
 OSS,COS等云端图片上传，
jpush极光推送，
批量邮件发送，多语言化，
灵活开启谷歌验证码验证。

### 导航栏目


 | [官网地址](http://www.luckyhhy.cn)
 | [TP6开发手册](https://www.kancloud.cn/manual/thinkphp6_0/1037479)
 | [服务器](https://promotion.aliyun.com/ntms/yunparter/invite.html?userCode=dligum2z)

- - -

### Stargazers over time
[![Stargazers over time](https://whnb.wang/img/luckygyl/jrk_blog)](https://whnb.wang/luckygyl/jrk_blog)


### 安装
使用git安装
~~~
git clone https://gitee.com/luckygyl/jrk_blog
~~~

1、sql 文件在 data 文件夹下面 <br>
2、修改 .env 里面的数据库配置  <br>
3、数据库导入 sql <br>
4、超管账号密码： jrkblog   jrkblog <br>
5、配置伪静态
6、后台-系统设置-常规设置-站点配置


### 伪静态配置
~~~~
location / {
   if (!-e $request_filename){
      rewrite  ^(.*)$  /index.php?s=$1  last;   break;
   }
}
~~~~

### 演示地址
http://www.lovegyl.cn

### 推荐环境
推荐使用 Nginx + php7.1(+) + mysql 

### 项目截图

![输入图片说明](https://images.gitee.com/uploads/images/2021/0901/163100_bbcb7f05_1513275.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0901/163124_5583828d_1513275.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0901/163156_54d5ae72_1513275.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0901/163524_95c5a7b7_1513275.png "屏幕截图.png")



### 特别鸣谢
1. [YFCMF](https://www.iuok.cn/)
2. [thinkphp](http://www.thinkphp.cn)

### 往期项目

1. [图标选择器](https://gitee.com/luckygyl/iconFonts)