<?php

return [
    'Keep login'                                             => '保持会话',
    'Forgot password'                                        => '忘记密码?',
    'Change password'                                        => '修改密码',
    'Please login first'                                     => '请登录后再操作',
    'Send verification code'                                 => '发送验证码',
    'Redirect now'                                           => '立即跳转',
    'Operation completed'                                    => '操作成功!',
    'Operation failed'                                       => '操作失败!',
    'Unknown data format'                                    => '未知的数据格式!',
    'Network error'                                          => '网络错误!',
    'Advanced search'                                        => '高级搜索',
    'Invalid parameters'                                     => '未知参数',
    'No results were found'                                  => '记录未找到',
    'Parameter %s can not be empty'                          => '参数%s不能为空',
    'Token verification error'                               => 'Token验证错误！',
    'You have no permission'                                 => '你没有权限访问',
    'An unexpected error occurred'                           => '发生了一个意外错误,程序猿正在紧急处理中',
    'This page will be re-directed in %s seconds'            => '页面将在 %s 秒后自动跳转',
];
