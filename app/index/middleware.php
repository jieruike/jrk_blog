<?php
/**
 * *
 *  * ============================================================================
 *  * Created by PhpStorm.
 *  * User: Ice
 *  * 邮箱: ice@sbing.vip
 *  * 网址: https://sbing.vip
 *  * Date: 2019/9/19 下午3:52
 *  * ============================================================================.
 */

return [
    app\common\middleware\FastInit::class,
    // Session初始化
    \think\middleware\SessionInit::class,
];
