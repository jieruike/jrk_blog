<?php
// +----------------------------------------------------------------------
// | 模板设置
// +----------------------------------------------------------------------
use think\facade\Env;
return [
    // 字符替换
    'tpl_replace_string' => [
        '__STA__' =>'/assets',
        '__JS__'     => '/assets/js', // 公共js
        '__FJS__'     => '/assets/js/frontend', // 前台js
        '__FCSS__'     => '/assets/css/frontend', //前台 css
        '__FIMG__'     => '/assets/img/frontend', // 图片
        '__PLUG__'     => '/plugs', //插件地址
        '__PUBLIC__'     => '/', //
        '__IMG__' =>'/assets/img'
    ]
];

