<?php

use think\facade\Route;

Route::rule('/', 'index/index/index');
//分类
Route::get('category', 'Categorys/index');
Route::get('cate_list/:id', 'Categorys/lists')->pattern(['id' => '[\w|\-\=]+']);

//标签
Route::get('tag', 'Tag/index');
Route::get('tag_show/:name', 'Tag/lists')->pattern(['name' => '[\w|\-]+']);

//文章详细
Route::get('show/:id', 'Index/show')->pattern(['id' => '[\w|\-\=]+']);

Route::get('about', 'About/index');

Route::get('archives', 'Archives/index');

Route::get('link', 'Link/index');

//搜索
Route::get('search', 'Index/search');


//miss 路由
Route::miss(function() {
    if(app()->request->isOptions()){
        return \think\Response::create('ok')->code(200)->header([
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Headers' => 'Authori-zation,Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-Requested-With',
            'Access-Control-Allow-Methods' => 'GET,POST,PATCH,PUT,DELETE,OPTIONS,DELETE',
        ]);
    }else{
        return \think\Response::create()->code(404);
    }
});