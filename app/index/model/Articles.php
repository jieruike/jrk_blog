<?php

namespace app\index\model;

use app\admin\model\article\Tags;
use app\admin\model\Category;
use app\common\traits\JumpReturn;
use think\facade\Db;
use think\Model;
use think\model\concern\SoftDelete;

class Articles extends Model
{
    use JumpReturn;

    use SoftDelete;

    // 表名
    protected $name = 'articles';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';


    /**
     * @return int
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/6/26 0026
     * @describe:
     */
    public function setCreateTimeAttr(){
        return time();
    }


    /**
     * @return int
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @describe:
     */
    public function setUpdateTimeAttr(){
        return time();
    }


    /**
     * @param $time
     * @return false|string
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @describe:
     */
    protected function getCreateTimeAttr($time){

        return date("Y-m-d H:i:s",$time);
    }


    public function cates()
    {
        return $this->hasOne(Category::class, "id", "category_id");
    }


    /**
     * @param $article_id
     * @return mixed
     * @author: Hhy <jackhhy520@qq.com>
     * @describe:获取文章的category_id
     */
    public static function getArticleCateId($article_id)
    {
        return self::where("id", $article_id)->value("category_id");
    }


    /**
     * @param $article_id
     * @return mixed
     * @author: Hhy <jackhhy520@qq.com>
     * @describe:更新文章评论数量
     */
    public static function updateArticleComments($article_id)
    {
        return self::where("id", $article_id)->inc('comment_num')->update();
    }


    public function getKeywordsAttr($value){
        $ids=@explode(',',$value);
        $name=Tags::where("id","in",$ids)->column("name");
        if (!empty($name)){
            return @implode(",",$name);
        }
        return "-";
    }


    /**
     * @param $article_id
     * @return mixed
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @describe:文章点击量自增1
     */
    public static function inc_article_hits($article_id)
    {
        return Articles::where("id", $article_id)->inc('hits', 1)->update();
    }


    /**
     * @param $article_id
     * @return mixed
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @describe:文章喜欢量自增1
     */
    public static function inc_article_love($article_id)
    {
        return Articles::where("id", $article_id)->inc('love', 1)->update();
    }


    //前端分类文章列表
    public static function Categarylists($id){
        $res= self::with("cates")->where("is_show","=",1)->where("category_id","=",$id)->order("createtime desc ")->paginate(6);
        return $res;
    }


    //前端标签文章列表
    public static function Taglists($id){
        $AID=self::whereFindInSet('keywords',$id)->column('id');
        $res= self::with("cates")->where("id","in",$AID)->order("createtime desc ")->paginate(6);
        return $res;
    }


    //首页推荐文章
    public static function getRecommend($limit){
        $res= self::with("cates")->where("is_show","=",'1')
            ->where("is_recommend","=",'1')
            ->order("createtime asc ")
            ->limit($limit)
            ->select();
        return $res;
    }


    //首页列表
    public static function indexList($limit=6){
        $res= self::with("cates")->where("is_show","=",'1')->where("is_recommend","=",'0')->order("createtime desc")->paginate($limit);
        return $res;
    }


    //搜索
    public static function getSearchList($key){
        $where = [];
        if ($key != '') {
            $where[] = ['title|description', 'like', "%" . $key . "%"];
        }
        $res=self::where($where)->order("createtime desc")->where("is_show",1)->field("id,url,title,description")->select()->toArray();
        if (empty($res)){
            return self::ajaxResult($res,0,0,"未查询到数据");
        }
        return self::ajaxResult($res,0,1,"询到数据成功");
    }



    //归档获取文章列表
    public static function getArcheve(){
        $res=self::with("cates")->where("is_show",1)->order("createtime desc")->paginate(8);
        if (!$res->isEmpty()){
            foreach ($res as $k=>$v){
                $t=strtotime($v['createtime']);
                $res[$k]['day']=date("d",$t);
                $mid=self::isMonthEnd($v['id']);
                $yid=self::isYearnd($v['id']);
                if ($mid==$v['id']){
                    $res[$k]['month']=date("m",$t);
                }
                if ($yid==$v['id']){
                    $res[$k]['year']=date("Y",$t);
                }
            }
        }
        return $res;
    }


    //获取当月最后一偏文章
    public static function isMonthEnd($aid){
        $info=self::where("id",$aid)->find()->toArray();
        $m=date("Y-m",strtotime($info['createtime']));
        return self::where("is_show",1)->whereMonth("createtime",$m)->order("id desc")->limit(1)->value("id");
    }


    //获取当年最后一偏文章
    public static function isYearnd($aid){
        $info=self::where("id",$aid)->find()->toArray();
        $m=date("Y",strtotime($info['createtime']));
        return self::where("is_show",1)->whereYear("createtime",$m)->order("id desc")->limit(1)->value("id");
    }




}