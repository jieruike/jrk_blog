<?php


namespace app\index\model;


use think\Model;

class Tags extends Model
{

    protected $name='tags';

    //获取标签及标签文章数量
    public function getTag(){
        $list = self::order("createtime desc")->select()->toArray();
        foreach ($list as $k=>$v){
           $list[$k]['articletag_count']= Articles::whereFindInSet('keywords',$v['id'])->count('id');
        }
        return $list;
    }


}