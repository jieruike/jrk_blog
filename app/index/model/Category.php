<?php


namespace app\index\model;


use think\Model;

class Category extends Model
{

    //文章分类
    protected $name = "category";

    //隐藏字段
    protected $hidden=['pid','description','updatetime','image'];


    public function articles(){
        return $this->hasMany(Articles::class,"category_id","id");
    }


    //查询所有的分类及文章数量
    public function category_nums(){
        return self::withCount("articles")->where("status",'1')->order("weigh desc")->select()->toArray();
    }
}