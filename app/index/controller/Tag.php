<?php

namespace app\index\controller;


use app\common\controller\Frontend;
use app\index\model\Articles;
use app\index\model\Tags;


class Tag extends Frontend
{

    protected $tag_name;

    public function _initialize()
    {
        parent::_initialize(); // TODO: Change the autogenerated stub

        $this->model=new Tags();

        $this->tag_name=$this->model->getTag();

        $this->assign("tag_num",$this->tag_name);
    }


    //标签
    public function index(){

        $arr=[];
        foreach ($this->tag_name as $k=>$v){
            $arr[$k]['text']=$v['name'];
            $arr[$k]['weight']=$v['articletag_count'];
            $arr[$k]['link']="/tag/".$v['name'];
        }
        $this->assign("tag",$arr);

        $this->assign("title","标签统计");
        return $this->fetch();
    }


    //标签列表
    public function lists(){
        $name=$this->request->param("name",null);

        $info=$this->model->where("name",$name)->find();

        if (!$info){
            $this->failed("参数错误");
        }
        $id=$info['id'];

        $list=Articles::Taglists($id);

        $page=$list->render();

        $this->assign(compact("id","name","list","page"));
        $this->assign("title",$info['name']."_列表");
        return $this->fetch();
    }



    //异步请求分页数据
    public function getList(){

        if ($this->request->isAjax()){
            $param=$this->request->param();
            return Articles::Taglists($param);
        }
    }
}