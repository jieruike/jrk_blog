<?php


namespace app\index\controller;

use app\index\model\Category;
use app\index\model\Tags;
use app\common\controller\Frontend;
use app\index\model\Articles;
use think\facade\Db;

class About extends Frontend
{
    public function _initialize()
    {
         parent::_initialize(); // TODO: Change the autogenerated stub
    }

    public function index(){

        //标签
        $tag=new Tags();
        $tag_data=$tag->getTag();
        $t_name=[];$t_num=[];
        if (!empty($tag_data)){
            foreach ($tag_data as $k=>$v){
                $t_name[$k]=$v['name'];
                $t_num[$k]=$v['articletag_count'];
            }
        }

        //文章分类
        $cat=new Category();
        $cat_data=$cat->category_nums();
        $a_name=[];$a_num=[];
        if (!empty($cat_data)){
            foreach ($cat_data as $k=>$v){
                $a_name[$k]=$v['name'];
                $a_num[$k]=$v['articles_count'];
            }
        }

        $this->wenzhang();

        //文章总是
        $article=Articles::count("id");

        //标签
        $tag=Tags::count("id");

        //分类
        $cate=Category::count("id");

        $this->assign(compact("t_name","t_num","a_name","a_num","article","tag","cate"));

        $this->assign("title","关于");
        return $this->fetch();


    }


    //文章统计
    public function wenzhang(){
        $yer=date("Y",time());
        $res=Db::name('articles')
            ->where('YEAR( FROM_UNIXTIME( createtime ) )',$yer)
            ->field('COUNT(*) AS number, MONTH( FROM_UNIXTIME( createtime ) ) AS month')
            ->group('MONTH( FROM_UNIXTIME( createtime ) )')
            ->select();

        $houdata=array();
        if ($res){
            for ($i=0;$i<12;$i++){
                $houdata[$i]=0;
                foreach ($res as $k=>$t){
                    if($t['month']==$i+1){
                        $houdata[$i]=(int)$t['number'];
                    }
                }
            }
        }else{
            for ($i=0;$i<12;$i++){
                $houdata[$i]=0;
            }
        }
        $this->assign("wenzhang",$houdata);
    }

}