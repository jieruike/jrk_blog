<?php

namespace app\common\library;


use app\admin\model\general\HhyConfig;

class Push
{
    /**
     * @var array Push的实例
     */
    public static $instance = [];
    /**
     * @var object 操作句柄
     */
    public static $handler;

/*    public function zhanzhang(){
        if (IS_POST){
            $action = $this->request->post("action");
            $urls = $this->request->post("urls");

            $urls = explode("\n", $urls);
            $urls = array_unique(array_filter($urls));
            if (!$urls) {
                $this->error("URL列表不能为空");
            }

            $result = false;
            if ($action == 'urls') {
                $result = Pushs::init(['type' => 'zhanzhang'])->realtime($urls);
            } elseif ($action == 'del') {
                $result = Pushs::init(['type' => 'zhanzhang'])->delete($urls);
            }
            if ($result) {
                $data = Pushs::init()->getData();
                $this->success("推送成功", null, $data);
            } else {
                $this->error("推送失败：" . Pushs::init()->getError());
            }
        }
    }*/


   /* public function xiongzhang()
    {
        $action = $this->request->post("action");
        $urls = $this->request->post("urls");
        $urls = explode("\n", $urls);
        $urls = array_unique(array_filter($urls));
        if (!$urls) {
            $this->error("URL列表不能为空");
        }
        $result = false;
        if ($action == 'urls') {
            $type = $this->request->post("type");
            if ($type == 'realtime') {
                $result = Pushs::init(['type' => 'xiongzhang'])->realtime($urls);
            } else {
                $result = Pushs::init(['type' => 'xiongzhang'])->history($urls);
            }
        } elseif ($action == 'del') {
            $result = Pushs::init(['type' => 'xiongzhang'])->delete($urls);
        }

        if ($result) {
            $data = Pushs::init()->getData();
            $this->success("推送成功", null, $data);
        } else {
            $this->error("推送失败：" . Pushs::init()->getError());
        }
    }*/



    /**
     * @param array $options
     * @param bool $name Push连接标识 true 强制重新初始化
     * @return mixed
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @name: connect
     * @describe: 连接Push驱动
     */
    public static function connect(array $options = [], $name = false)
    {
        $type = !empty($options['type']) ? $options['type'] : 'zhanzhang';
        $push= HhyConfig::configList('push');
        $config=[
            'xiongzhang'=>[
                'appid'=>$push["xzappid"],
                'token'=>$push["xztoken"]
            ],
            'zhanzhang'=>[
                'site'=>$push["zz_site"],
                'token'=>$push["zz_token"]
            ]
        ];

        $type = strtolower($type);

        $options = array_merge($options, isset($config[$type]) ? $config[$type] : []);

        if (false === $name) {
            $name = md5(serialize($options));
        }

        if (true === $name || !isset(self::$instance[$name])) {
            $class = false === strpos($type, '\\') ?
                '\\app\\common\\library\\push\\driver\\' . ucwords($type) :
                $type;

            if (true === $name) {
                return new $class($options);
            }

            self::$instance[$name] = new $class($options);
        }

        return self::$instance[$name];
    }

    /**
     * @param array $options
     * @return mixed|object
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @name: init
     * @describe:自动初始化Push
     */
    public static function init(array $options = [])
    {
        if (is_null(self::$handler)) {
            self::$handler = self::connect($options);
        }

        return self::$handler;
    }

    /**
     * @param $urls
     * @return mixed
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @name: realtime
     * @describe:
     */
    public static function realtime($urls)
    {
        return self::init()->realtime($urls);
    }

    /**
     * @param $urls
     * @return mixed
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @name: history
     * @describe:推送历史链接
     */
    public static function history($urls)
    {
        return self::init()->history($urls);
    }

    /**
     * @param $urls
     * @return mixed
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @name: delete
     * @describe: 删除链接
     */
    public static function delete($urls)
    {
        return self::init()->delete($urls);
    }
}