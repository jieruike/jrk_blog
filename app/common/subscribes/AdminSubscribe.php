<?php



namespace app\common\subscribes;

use app\admin\model\AdminUser;


class AdminSubscribe extends Base
{

    /**
     * @param $event
     * @author: Hhy <jackhhy520@qq.com>
     * @describe:用户登录后
     */
    public function onAdminLoginAfter($event){
         list($data)=$event;
         AdminUser::update($data);
    }




}