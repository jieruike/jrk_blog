<?php
// +----------------------------------------------------------------------
// | Created by PHPstorm: [ JRK丶Admin ]
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 [LuckyHHY] All rights reserved.
// +----------------------------------------------------------------------
// | SiteUrl: http://www.luckyhhy.cn
// +----------------------------------------------------------------------
// | Author: LuckyHhy <jackhhy520@qq.com>
// +----------------------------------------------------------------------
// | Date: 2020/6/25 0025
// +----------------------------------------------------------------------
// | Description:  
// +----------------------------------------------------------------------

namespace app\common\listener;


class AppInit
{
    public function handle(){
        $this->initSystemConst();
    }

    /**
     * @author: LuckyHhy <jackhhy520@qq.com>
     * @date: 2020/6/25 0025
     * @describe:初始化系统常量
     */
    private function initSystemConst(){
        !defined('DS') && define('DS', DIRECTORY_SEPARATOR);
        !defined('DS_CONS') && define('DS_CONS', '\\');
    }

}