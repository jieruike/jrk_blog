<?php

return [
    'Begin time'                                  => '开始时间',
    'End time'                                    => '结束时间',
    'Create time'                                 => '创建时间',
    'Flag'                                        => '标志',
    'Please login first'                          => '请登录后操作',
    'Redirect now'                                => '立即跳转',
    'Operation completed'                         => '操作成功!',
    'Operation failed'                            => '操作失败!',
    'Unknown data format'                         => '未知的数据格式!',
    'Network error'                               => '网络错误!',
    'Advanced search'                             => '高级搜索',
    'Invalid parameters'                          => '未知参数',
    'No results were found'                       => '记录未找到',
    'Parameter %s can not be empty'               => '参数%s不能为空',
    'You have no permission'                      => '你没有权限访问',
    'An unexpected error occurred'                => '发生了一个意外错误,程序猿正在紧急处理中',
    'This page will be re-directed in %s seconds' => '页面将在 %s 秒后自动跳转',
];
