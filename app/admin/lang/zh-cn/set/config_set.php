<?php

return [
    'Name'              => '配置标识',
    'Title'             => '名称',
    'Message'           => '描述',
    'Background'        => '背景颜色',
    'Background_button' => '按钮背景色',
    'Background_color'  => '按钮字体颜色',
    'Icon' =>'图标',
    'Status'            => '状态',
    'Status 0'          => '关闭',
    'Status 1'          => '开启'
];
