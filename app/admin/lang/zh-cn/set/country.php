<?php

return [
    'E_name'   => '国家英文名大写',
    'Code'     => '国家代码',
    'C_name'   => '国家中文名称',
    'S_name'   => '国家英文名小写',
    'Status'   => '状态',
    'Status 1' => '启用',
    'Status 0' => '关闭'
];
