<?php

return [
    'Name'              => '海报名称',
    'Image'             => '背景图片',
    'Qrcode_size'       => '二维码大小',
    'Qrcode_position'   => '二维码坐标',
    'Is_avatar'         => '是否显示头像',
    'Is_avatar 0'       => '否',
    'Is_avatar 1'       => '是',
    'Avatar_size'       => '头像大小',
    'Avatar_position'   => '头像坐标',
    'Is_font'           => '是否显示邀请码',
    'Is_font 0'         => '否',
    'Is_font 1'         => '是',
    'Font_size'         => '邀请码设置',
    'Font_position'     => '邀请码坐标',
    'Is_nickname'       => '是否显示昵称',
    'Is_nickname 0'     => '否',
    'Is_nickname 1'     => '是',
    'Nickname_size'     => '昵称设置',
    'Nickname_position' => '昵称坐标',
    'Weigh'             => '权重',
    'Status'            => '状态',
    'Status hidden'     => '隐藏',
    'Status normal'     => '正常',
    'Createtime'        => '创建时间',
    'Updatetime'        => '更新时间'
];
