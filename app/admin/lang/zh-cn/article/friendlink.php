<?php

return [
    'Name'     => '链接名称',
    'Url'      => '链接地址',
    'Status'   => '状态',
    'Status 1' => '启用',
    'Status 0' => '关闭'
];
