<?php

namespace app\admin\event;



use think\facade\Env;

class AdminLog
{
    public function handle()
    {
        if (request()->isPost() && config('fastadmin.auto_record_log')) {
            \app\admin\model\AdminLog::record();
        }
    }
}
