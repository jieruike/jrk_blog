<?php

namespace app\admin\controller\article;

use app\common\controller\Backend;
use think\Exception;
use think\exception\ValidateException;
use think\facade\Db;

/**
 * 文章标签
 *
 * @icon fa fa-tags
 */
class Tags extends Backend
{
    
    /**
     * Tags模型对象
     * @var \app\admin\model\article\Tags
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->modelValidate=true;
        $this->model = new \app\admin\model\article\Tags;
        $this->view->assign("statusList", $this->model->getStatusList());
    }


 /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

        /**
         * 添加
         */
        public function add()
        {
            if ($this->request->isPost()) {
                $params = $this->request->post('row/a');
                if ($params) {
                    $params = $this->preExcludeFields($params);

                    if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                        $params[$this->dataLimitField] = $this->auth->id;
                    }
                    $result = false;
                    $this->model->startTrans();

                    try {
                        //是否采用模型验证
                        if ($this->modelValidate) {
                            $name     = str_replace('\\model\\', '\\validate\\', get_class($this->model));
                            $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name.'.add' : $name) : $this->modelValidate;
                            validate($validate)->scene($this->modelSceneValidate ? 'edit' : $name)->check($params);
                        }
                        $result = $this->model->save($params);
                        $this->model->commit();
                    } catch (ValidateException $e) {
                        $this->model->rollback();
                        $this->error($e->getMessage());
                    } catch (\PDOException $e) {
                        $this->model->rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        $this->model->rollback();
                        $this->error($e->getMessage());
                    }
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were inserted'));
                    }
                }
                $this->error(__('Parameter %s can not be empty', ''));
            }

            return $this->view->fetch();
        }


         /**
             * 编辑
             */
            public function edit($ids = null)
            {
                $row = $this->model->get($ids);
                if (!$row) {
                    $this->error(__('No Results were found'));
                }
                $adminIds = $this->getDataLimitAdminIds();
                if (is_array($adminIds)) {
                    if (!in_array($row[$this->dataLimitField], $adminIds)) {
                        $this->error(__('You have no permission'));
                    }
                }
                if ($this->request->isPost()) {
                    $params = $this->request->post('row/a');
                    if ($params) {
                        $params = $this->preExcludeFields($params);
                        $result = false;
                        $this->model->startTrans();

                        try {
                            //是否采用模型验证
                            if ($this->modelValidate) {
                                $name     = str_replace('\\model\\', '\\validate\\', get_class($this->model));
                                $validate = is_bool($this->modelValidate) ? $name : $this->modelValidate;
                                $pk       = $row->getPk();
                                if (!isset($params[$pk])) {
                                    $params[$pk] = $row->$pk;
                                }
                                validate($validate)->scene($this->modelSceneValidate ? 'edit' : $name)->check($params);
                            }
                            $result = $row->save($params);
                            $this->model->commit();
                        } catch (ValidateException $e) {
                            $this->model->rollback();
                            $this->error($e->getMessage());
                        } catch (\PDOException $e) {
                            $this->model->rollback();
                            $this->error($e->getMessage());
                        } catch (Exception $e) {
                            $this->model->rollback();
                            $this->error($e->getMessage());
                        }
                        if ($result !== false) {
                            $this->success();
                        } else {
                            $this->error(__('No rows were updated'));
                        }
                    }
                    $this->error(__('Parameter %s can not be empty', ''));
                }
                $this->view->assign('row', $row);

                return $this->view->fetch();
            }

}
