<?php

namespace app\admin\controller\general;

use app\admin\model\set\ConfigSet;
use app\common\controller\Backend;
use think\facade\Db;
use think\Exception;
/**
 * 系统配置
 *
 * @icon fa fa-circle-o
 */
class HhyConfig extends Backend
{

    /**
     * HhyConfig模型对象
     * @var \app\admin\model\general\HhyConfig
     */
    protected $model = null;


    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $noNeedRight = ['*'];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\general\HhyConfig;

    }


    public function import()
    {
        parent::import();
    }

    public function index()
    {
       $all= ConfigSet::where("status",1)->select();
       foreach ($all as $k=>$v){
           $all[$k]['background']="linear-gradient(180deg, {$v['background']} 0%, #18bc9c 100%)";
           //$all[$k]['button']="background:'{$v['background_button']}',color:'{$v['background_color']}'";
           $ar[0]['background']='#DBE2F6';
           $ar[1]['color']='#1B5EE7';
           $all[$k]['button']=$ar;
       }
       $this->assignconfig('config_list',$all);
       return $this->fetch();
    }


    public function platform($type){
       // halt(123);
        if ($this->request->isPost()) {
            $row = $this->request->post("row/a", [], 'trim');
            // dump($row);die;
            if ($row) {
                try {
                    $config = $this->model->where('name' , $type)->find();
                    if (!$config){
                        $value = json_encode($row);
                        $this->model->save(['name'=>$type,'value'=>$value]);
                    }else{

                        //提现
                        if ($type=="shop"){
                            if (!isset($row['grab'])){
                                $this->error('请添加抢单金额区间配置');
                            }
                            //判断时间段冲突，开始
                            $arr = [];
                            $i = 0;
                            foreach ($row['grab'] as $k => $v) {

                                if (intval($v['end']) < intval($v['start'])) {
                                    $this->error('起始金额不能大于终止金额');
                                    break;
                                }
                                $arr[$i]['start'] = $v['start'];
                                $arr[$i]['end'] = $v['end'];
                                $i++;
                            }
                            if (!empty($arr)) {
                                // dump($arr);
                                //判断段冲突
                                $cross = self::MoneyCross($arr);
                                if ($cross) {
                                    $this->error('有冲突的金额段');
                                }
                            }else{
                                $this->error('请添加抢单金额区间配置');
                            }
                            unset($i,$arr);
                        }

                        if ($type=="task"){
                            if (!isset($row['fan'])){
                                $this->error('请添加金额区间配置');
                            }
                            //判断时间段冲突，开始
                            $arr = [];
                            $i = 0;
                            foreach ($row['fan'] as $k => $v) {

                                if (intval($v['end']) < intval($v['start'])) {
                                    $this->error('起始金额不能大于终止金额');
                                    break;
                                }
                                $arr[$i]['start'] = $v['start'];
                                $arr[$i]['end'] = $v['end'];
                                $i++;
                            }
                            if (!empty($arr)) {
                                // dump($arr);
                                //判断段冲突
                                $cross = self::MoneyCross($arr);
                                if ($cross) {
                                    $this->error('有冲突的金额段');
                                }
                            }else{
                                $this->error('请添加金额区间配置');
                            }
                            unset($i,$arr);
                        }

                        $config->value = json_encode($row);
                        $config->save();
                    }
                } catch (\Exception $e) {
                    $this->error($e->getMessage());
                }
                $this->success();
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }


        $config = $this->model->where(['name' => $type])->value('value');
        $config = json_decode($config, true);

        if ($type=="shop"){
            $config['grab']=json_encode($config['grab']);
        }
        if ($type=="task"){
            $config['fan']=json_encode($config['fan']);
        }

        $this->assign('row', $config);
        return $this->view->fetch();
    }


    /**
     * @param array $data
     * @return bool
     * @describe:判断多个金额段
     */
    protected static function MoneyCross(array $data)
    {
        $dfiled = array_column($data, 'start');
        // 按开始时间排序
        array_multisort($dfiled, SORT_ASC, $data);
        // 冒泡判断是否满足时间段重合的条件
        $num = count($data);
        for ($i = 1; $i < $num; $i++) {
            $pre = $data[$i - 1];
            $current = $data[$i];
            if (intval($pre['start']) <= intval($current['end']) && intval($current['start']) <= intval($pre['end'])) {
                return true;
            }
        }
        return false;
    }

}
