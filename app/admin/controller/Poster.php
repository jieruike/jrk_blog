<?php

namespace app\admin\controller;

use addons\poster\logic\PosterLogic;
use app\common\controller\Backend;

/**
 * 海报管理
 *
 * @icon fa fa-circle-o
 */
class Poster extends Backend
{
    
    /**
     * Poster模型对象
     * @var \app\admin\model\Poster
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Poster;
        $this->view->assign("isAvatarList", $this->model->getIsAvatarList());
        $this->view->assign("isFontList", $this->model->getIsFontList());
        $this->view->assign("isNicknameList", $this->model->getIsNicknameList());
        $this->view->assign("statusList", $this->model->getStatusList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = false;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
                $row->visible(['id','name','image','is_avatar','is_font','is_nickname','weigh','status','createtime','updatetime']);
                
            }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }


    /**
     * 清空海报
     */
    public function clear()
    {

        if ($this->request->isAjax()) {
            $dirName = root_path('public').'qrcodes';
            if (file_exists($dirName)) {
                rmdirs($dirName);
            }

            $this->success('海报已清除');
        }


    }


}
