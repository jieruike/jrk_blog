<?php
// +----------------------------------------------------------------------
// | Created by PHPstorm: [ JRKBlog ]
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 [技术小贺 ] All rights reserved.
// +----------------------------------------------------------------------
// | SiteUrl: http://www.lovegyl.cn
// +----------------------------------------------------------------------
// | Author: 技术丶小贺
// +----------------------------------------------------------------------
// | Date: 2021-09-01 14:22:59
// +----------------------------------------------------------------------
// | Description:
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\common\controller\Backend;
use think\Exception;
use think\exception\ValidateException;
use think\facade\Db;

/**
 * 测试管理
 *
 * @icon fa fa-circle-o
 */
class Test extends Backend
{
    
    /**
     * Test模型对象
     * @var \app\admin\model\Test
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->modelValidate=true;
        $this->model = new \app\admin\model\Test;
        $this->view->assign("weekList", $this->model->getWeekList());
        $this->view->assign("flagList", $this->model->getFlagList());
        $this->view->assign("genderdataList", $this->model->getGenderdataList());
        $this->view->assign("hobbydataList", $this->model->getHobbydataList());
        $this->view->assign("statusList", $this->model->getStatusList());
        $this->view->assign("stateList", $this->model->getStateList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
 /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

        /**
         * 添加
         */
        public function add()
        {
            if ($this->request->isPost()) {
                $params = $this->request->post('row/a');
                if ($params) {
                    $params = $this->preExcludeFields($params);

                    if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                        $params[$this->dataLimitField] = $this->auth->id;
                    }
                    $result = false;
                    $this->model->startTrans();

                    try {
                        //是否采用模型验证
                        if ($this->modelValidate) {
                            $name     = str_replace('\\model\\', '\\validate\\', get_class($this->model));
                            $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name.'.add' : $name) : $this->modelValidate;
                            validate($validate)->scene($this->modelSceneValidate ? 'edit' : $name)->check($params);
                        }
                        $result = $this->model->save($params);
                        $this->model->commit();
                    } catch (ValidateException $e) {
                        $this->model->rollback();
                        $this->error($e->getMessage());
                    } catch (\PDOException $e) {
                        $this->model->rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        $this->model->rollback();
                        $this->error($e->getMessage());
                    }
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were inserted'));
                    }
                }
                $this->error(__('Parameter %s can not be empty', ''));
            }

            return $this->view->fetch();
        }


         /**
             * 编辑
             */
            public function edit($ids = null)
            {
                $row = $this->model->get($ids);
                if (!$row) {
                    $this->error(__('No Results were found'));
                }
                $adminIds = $this->getDataLimitAdminIds();
                if (is_array($adminIds)) {
                    if (!in_array($row[$this->dataLimitField], $adminIds)) {
                        $this->error(__('You have no permission'));
                    }
                }
                if ($this->request->isPost()) {
                    $params = $this->request->post('row/a');
                    if ($params) {
                        $params = $this->preExcludeFields($params);
                        $result = false;
                        $this->model->startTrans();

                        try {
                            //是否采用模型验证
                            if ($this->modelValidate) {
                                $name     = str_replace('\\model\\', '\\validate\\', get_class($this->model));
                                $validate = is_bool($this->modelValidate) ? $name : $this->modelValidate;
                                $pk       = $row->getPk();
                                if (!isset($params[$pk])) {
                                    $params[$pk] = $row->$pk;
                                }
                                validate($validate)->scene($this->modelSceneValidate ? 'edit' : $name)->check($params);
                            }
                            $result = $row->save($params);
                            $this->model->commit();
                        } catch (ValidateException $e) {
                            $this->model->rollback();
                            $this->error($e->getMessage());
                        } catch (\PDOException $e) {
                            $this->model->rollback();
                            $this->error($e->getMessage());
                        } catch (Exception $e) {
                            $this->model->rollback();
                            $this->error($e->getMessage());
                        }
                        if ($result !== false) {
                            $this->success();
                        } else {
                            $this->error(__('No rows were updated'));
                        }
                    }
                    $this->error(__('Parameter %s can not be empty', ''));
                }
                $this->view->assign('row', $row);

                return $this->view->fetch();
            }

}
