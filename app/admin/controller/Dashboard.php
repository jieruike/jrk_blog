<?php
/**
 * *
 *  * ============================================================================
 *  * Created by PhpStorm.
 *  * User: Ice
 *  * 邮箱: ice@sbing.vip
 *  * 网址: https://sbing.vip
 *  * Date: 2019/9/19 下午3:33
 *  * ============================================================================.
 */

namespace app\admin\controller;

use app\admin\model\article\Articles;
use app\admin\model\article\Friendlink;
use app\admin\model\article\Mingju;
use app\admin\model\article\Tags;
use jrk\Time;
use app\common\controller\Backend;
use think\facade\Db;

/**
 * 控制台.
 *
 * @icon fa fa-dashboard
 * @remark 用于展示当前系统中的统计数据、统计报表及重要实时数据
 */
class Dashboard extends Backend
{

    protected $noNeedRight = ['getIncomeStatisticsData','tong','table_one'];
    /**
     * 查看.
     */
    public function index()
    {

        $this->view->assign([
            'friendlink'        => Friendlink::where('status',1)->count('id'),
            'articles'       =>  Articles::count('id'),
            'tag'       => Tags::count('id'),
            'mingju' => Mingju::count('id')
        ]);

        list($category, $incomeData) = $this->getIncomeStatisticsData();

        $this->assignconfig('category', $category);
        $this->assignconfig('incomeData', $incomeData);

        return $this->view->fetch();

    }


    public function table_one(){

        if ($this->request->isPost()) {
            $date = $this->request->post('date', '');

            list($category, $incomeData) = $this->getIncomeStatisticsData($date);
            $statistics = ['category' => $category, 'incomeData' => $incomeData];

            $this->success('', '', $statistics);
        }

    }


    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @describe:
     */
    protected function getIncomeStatisticsData($date = '')
    {
        if ($date) {
            list($start, $end) = explode(' - ', $date);
            $starttime = strtotime($start);
            $endtime = strtotime($end);
        } else { // 默认是当月
            $starttime = Time::BeginThisMonth(false);
            $endtime = Time::EndThisMonth(false);
        }
        $totalseconds = $endtime - $starttime;

        $format = '%Y-%m-%d';
        if ($totalseconds > 86400 * 30 * 2) { // 大于两个月，则横坐标为以月为粒度 形式'Y-m'
            $format = '%Y-%m';
        } else {
            if ($totalseconds > 86400) { // 小于两个月 且大于一天，则横坐标以天为粒度 形式'Y-m-d'
                $format = '%Y-%m-%d';
            } else { // 小于一天，则横坐标为以小时为粒度 形式'H:00'
                $format = '%H:00';
            }
        }
        //
       $orderList =Db::name("articles")
            ->where('createtime', 'between time', [$starttime, $endtime])
            ->where('is_show', '=','1')
            ->field('createtime,COUNT(*) AS amount, DATE_FORMAT(FROM_UNIXTIME(createtime), "' . $format . '") AS pay_date')
            ->group('DATE_FORMAT(FROM_UNIXTIME(createtime), "' . $format . '")')
            ->select();

        if ($totalseconds > 84600 * 30 * 2) { // 大于两个月，则横坐标为以月为粒度 形式'Y-m'
            $starttime = strtotime('last month', $starttime);
            while (($starttime = strtotime('next month', $starttime)) <= $endtime) {
                $column[] = date('Y-m', $starttime);
            }
        } else {
            if ($totalseconds > 86400) { // 小于两个月 且大于一天，则横坐标以天为粒度 形式'Y-m-d'
                for ($time = $starttime; $time <= $endtime;) {
                    $column[] = date("Y-m-d", $time);
                    $time += 86400;
                }
            } else { // 小于一天，则横坐标为以小时为粒度 形式'H:00'
                for ($time = $starttime; $time <= $endtime;) {
                    $column[] = date("H:00", $time);
                    $time += 3600;
                }
            }
        }

        // 成功订单数
        $list = array_fill_keys($column, 0);
        foreach ($orderList as $k=>$v){
            $list[$v['pay_date']] = round($v['amount'], 2);
        }

        $category = array_keys($list);

        $incomeData = array_values($list);

        return [$category, $incomeData];
    }


}
