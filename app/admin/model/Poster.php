<?php

namespace app\admin\model;

use app\common\model\BaseModel;


class Poster extends BaseModel
{

    

    

    // 表名
    protected $name = 'poster';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_avatar_text',
        'is_font_text',
        'is_nickname_text',
        'status_text'
    ];
    
    /**
    * @param Model $row
    */
    protected static function onAfterInsert($row){
        $pk = $row->getPk();
        $row->where($pk, $row[$pk])->update(['weigh' => $row[$pk]]);
    }

    
    public function getIsAvatarList()
    {
        return ['0' => __('Is_avatar 0'), '1' => __('Is_avatar 1')];
    }

    public function getIsFontList()
    {
        return ['0' => __('Is_font 0'), '1' => __('Is_font 1')];
    }

    public function getIsNicknameList()
    {
        return ['0' => __('Is_nickname 0'), '1' => __('Is_nickname 1')];
    }

    public function getStatusList()
    {
        return ['hidden' => __('Status hidden'), 'normal' => __('Status normal')];
    }


    public function getIsAvatarTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_avatar']) ? $data['is_avatar'] : '');
        $list = $this->getIsAvatarList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsFontTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_font']) ? $data['is_font'] : '');
        $list = $this->getIsFontList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsNicknameTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_nickname']) ? $data['is_nickname'] : '');
        $list = $this->getIsNicknameList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
