<?php

namespace app\admin\model\article;

use app\common\model\BaseModel;
use think\model\concern\SoftDelete;

class Articles extends BaseModel
{

    use SoftDelete;

    // 表名
    protected $name = 'articles';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'is_tui_text',
        'is_recommend_text',
        'is_top_text',
        'is_show_text',
        'status_text'
    ];
    

    
    public function getIsTuiList()
    {
        return ['0' => __('Is_tui 0'), '1' => __('Is_tui 1'), '2' => __('Is_tui 2')];
    }

    public function getIsRecommendList()
    {
        return ['1' => __('Is_recommend 1'), '0' => __('Is_recommend 0')];
    }

    public function getIsTopList()
    {
        return ['1' => __('Is_top 1'), '0' => __('Is_top 0')];
    }

    public function getIsShowList()
    {
        return ['1' => __('Is_show 1'), '0' => __('Is_show 0')];
    }

    public function getStatusList()
    {
        return ['1' => __('Status 1'), '0' => __('Status 0')];
    }


    public function getIsTuiTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_tui']) ? $data['is_tui'] : '');
        $list = $this->getIsTuiList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsRecommendTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_recommend']) ? $data['is_recommend'] : '');
        $list = $this->getIsRecommendList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsTopTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_top']) ? $data['is_top'] : '');
        $list = $this->getIsTopList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsShowTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_show']) ? $data['is_show'] : '');
        $list = $this->getIsShowList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }



    public function banklist(){
        $data= Tags::all();
        $arr=[];
        $ids=[];
        foreach ($data as $k=>$v){
            $arr[$v['id']]=$v['name'];
            $ids[]=$v['id'];
        }
        return  ['arr'=>$arr,'bids'=>$ids];
    }


    public function category()
    {
        return $this->belongsTo('app\admin\model\Category', 'category_id', 'id')->visible(['name'])->joinType('LEFT');
    }
}
