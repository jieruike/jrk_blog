<?php

namespace app\admin\model\general;

use think\Model;


class HhyConfig extends Model
{

    // 表名
    protected $name = 'hhy_config';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];



    public static function configList($type='all'){
        $data= self::select();
        $arr=[];
        foreach ($data as $k=>$v){
            $arr[$v['name']]=array_merge(json_decode($v['value'],true),['title'=>$v['title']]);
        }
        return isset($arr[$type])?$arr[$type]:$arr;
    }










}
