<?php

namespace app\admin\model\set;

use app\common\model\BaseModel;


class ConfigSet extends BaseModel
{
    // 表名
    protected $name = 'hhy_config_set';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'icon_text',
        'status_text'
    ];
    

    
    public function getIconList()
    {
        return ['huodong' => __('Huodong'), 'order-icon' => __('Order-icon'), 'score-icon' => __('Score-icon'), 'services-icon' => __('Services-icon'), 'share-icon' => __('Share-icon'), 'shopro-icon' => __('Shopro-icon'), 'user-icon' => __('User-icon'), 'wallet-icon' => __('Wallet-icon'), 'withdraw-icon' => __('Withdraw-icon')];
    }

    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1')];
    }


    public function getIconTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['icon']) ? $data['icon'] : '');
        $list = $this->getIconList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }





}
