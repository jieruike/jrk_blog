<?php

namespace app\admin\validate\set;

use think\Validate;

class ConfigSet extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'name' => 'require|unique:hhy_config_set',
    ];
    /**
     * 提示消息
     */
    protected $message = [
        'name.unique' => '名称已存在'
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => ['name'],
        'edit' => ['name'],
    ];

    
}
